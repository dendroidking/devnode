import React from "react";
import { Link} from "react-router-dom";

const DashboardActions = () => {
    return (
        <div class='dash-buttons'>
            <Link to='/edit-profile' class='btm btn-light'>
                <i class='fas fa-user-circle text-primary'/> Edit Profile
            </Link>
            <Link to='/add-experience' class='btm btn-light'>
                <i class='fas fa-black-tie text-primary'/> Add Experience
            </Link>
            <Link to='/add-education' class='btm btn-light'>
                <i class='fas fa-graduation-cap text-primary'/> Add Education
            </Link>
        </div>
    )
}

export default DashboardActions